export type Tokens = {
  access_token: string;
  refresh_token: string;
};

export type JwtPayload = {
  username: string;
  password: string;
};

export type JwtPayloadWithRt = JwtPayload & { refresh_token: string };

export interface User {
  username: string;
  password: string;
}
