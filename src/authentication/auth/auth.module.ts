import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AtStrategy } from './strategies/at.strategy';
import { RtStrategy } from './strategies/rt.strategy';
import { JwtModule } from '@nestjs/jwt';
import { AuthenticationService } from './auth.service';
import { RedisService } from 'src/redis/redis.service';
import { RedisModule } from 'src/redis/redis.module';

@Module({
  imports: [RedisModule,
    // Other imports...
    JwtModule.register({
      secret: 'at-secret', // Replace with your access token secret key
      signOptions: { expiresIn: '15m' }, // Access token expiration
    }),
    JwtModule.register({
      // name: 'refresh',
      secret: 'rt-secret', // Replace with your refresh token secret key
      signOptions: { expiresIn: '1d' }, // Refresh token expiration
    }),
  ],
  providers: [AuthenticationService, AtStrategy, RtStrategy,RedisService],
  controllers: [AuthController],
  exports: [AuthenticationService]
})
export class AuthModule {}
