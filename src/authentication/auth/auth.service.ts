import {
  Injectable,
  UnauthorizedException,
  NotFoundException,
  ForbiddenException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as jwt from 'jsonwebtoken';
import { PrismaService } from '../../prisma/prisma.service';
import * as bcrypt from 'bcrypt';
// import { Redis } from 'ioredis';
import { RedisService } from '../../redis/redis.service';
import { SignInDto } from './authDto/authdto';
import { Tokens } from '../types/dto.type';

@Injectable()
export class AuthenticationService {
  constructor(
    private readonly prismaService: PrismaService,
    private readonly jwtService: JwtService,
    private readonly redisService: RedisService,
  ) {}

  async signin({ username, password }: SignInDto) {
    const response = await this.prismaService.user_auth.findUnique({
      where: { username },
    });

    const passwordMatches = await bcrypt.compare(password, response.hashedPassword);

    console.log({passwordMatches})

    if (!response || !passwordMatches ) {
      throw new UnauthorizedException('Invalid credentials');
    }
    const tokens = await this.generateTokens(
      response.id,
      username,
      // password
    );
    // console.log({ tokens });
    const redisresult = await this.redisService.saveJwtToken(
      username,
      tokens.refresh_token,
    );
    console.log({ redisresult });
    await this.updateRefreshToken(username, tokens.refresh_token);
    const data = {
      user: {id: response.id,
        username,},
      backendTokens: tokens,
    };
    return { data };
  }
  async updateRefreshToken(username: string, refresh_token: string) {

    const hashedToken = await bcrypt.hash(refresh_token, 10);
    const user = await this.prismaService.user_auth.update({
      where: {
        username: username,
      },
      data: {
        refresh_token: hashedToken,
      },
    });
    return { user };
  }

  async generateTokens(
    id: number,
    username: string,
    // password: string,
  ): Promise<Tokens> {
    const expiresIn = 60 * 15; // 5 MINUTES
    const [at, rt] = await Promise.all([
      // access token
      this.jwtService.signAsync(
        {
          username,
          id,
        },
        {
          secret: 'at-secret',
          expiresIn: expiresIn, //expire in 15 min
        },
      ),
      // refresh token
      this.jwtService.signAsync(
        {
          username,
          id, 
        },
        {
          secret: 'rt-secret',
          expiresIn: 60 * 60 * 24, // for one day
        },
      ),
    ]);

    const return_data = {
      access_token: at,
      refresh_token: rt,
      expiresIn: expiresIn,
    };

    return return_data;
  }

  async isTokenExpired(token: string): Promise<boolean> {
    try {
      const decodedToken = (await jwt.verify(token, 'at-secret')) as {
        exp: number;
      };
      const currentTimestamp = Math.floor(Date.now() / 1000);
      return decodedToken.exp < currentTimestamp;
    } catch (error) {
      if (error instanceof jwt.TokenExpiredError) {
        return true;
      }
      return true;
    }
  }

  async refresh(username: string, refresh_token: string) {
    const user = await this.prismaService.user_auth.findUnique({
      where: {
        username: username,
      },
    });
    if (!user)
      throw new NotFoundException({
        message: 'There is no user on this credentials',
      });
    const storedRefreshToken = await this.redisService.getRefreshToken(
      username,
    );

    console.log({ storedRefreshToken });

    const rtMatches = await bcrypt.compare(refresh_token, user.refresh_token);

    if (!rtMatches)
      throw new ForbiddenException({ message: "RefreshToken didn't match" });

    const tokens = await this.generateTokens(
      user.id,
      user.username,
      // user.password,
    );
    await this.updateRefreshToken(user.username, tokens.refresh_token);
    await this.redisService.saveJwtToken(username, tokens.refresh_token);
    return tokens;
  }

  async logout(clientId: number) {
    try {
      const client = await this.prismaService.user_auth.update({
        where: {
          id: clientId,
          refresh_token: {
            not: null,
          },
        },
        data: {
          refresh_token: null,
        },
      });
      return 'logged out successfully';
    } catch (error) {
      throw error;
    }
  }
}
