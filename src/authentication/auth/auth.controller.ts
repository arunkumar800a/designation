import { Body, Controller, Post, Query, Req } from '@nestjs/common';
import { User } from 'src/common/decorators/user.decorator';
import { Request } from 'express';
import { Public } from 'src/common/decorators/public.decorator';
import { AuthenticationService } from './auth.service';
import { ApiBody, ApiOperation, ApiTags } from '@nestjs/swagger';
import { SignInDto } from './authDto/authdto';

@ApiTags('Authentication')
@Controller('auth')
export class AuthController {

    constructor(private readonly authService: AuthenticationService){}

    @ApiOperation({ summary: 'Sigin and get tokens' })
    @Public()
    @Post('signin')
    // @ApiBody({ type: SignInDto }) // Specify request body
    signin(@Query() user: SignInDto){ 
        return this.authService.signin(user)
    }


    @ApiOperation({ summary: 'Logout' })
    @Post('logout')
    logout(@User() user){
        const clientId = user.id
        return this.authService.logout(clientId)
    }

    @ApiOperation({ summary: 'get new Refresh and Access Tokens' })
    @Public()
    @Post('refresh')
    refresh(@Req() req: Request, @User() user){
        const refreshToken = req.headers?.authorization?.split(' ')[1];
        return this.authService.refresh(user.sub, refreshToken)
    }
}
