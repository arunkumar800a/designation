import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SignInDto {
  @ApiProperty({
    required: true,
    description: 'User Name ',
    example: 'Arun',
  })
  @IsNotEmpty()
  username: string;

  @ApiProperty({
    required: true,
    description: 'User Password',
    example: '12345',
  })
  @IsNotEmpty()
  password: string;
}
