import {
  Body,
  Controller,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { UserService } from './user.service';
import { ApiBearerAuth, ApiBody, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import { createUserDto } from './user dtos/userDto';

@ApiTags('User')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiOperation({ summary: 'Create a new User' })
  // @ApiBody({ type: createUserDto }) // Specify request body
  @Post('createUser')
  async createUser(@Query() data: createUserDto) {
    return await this.userService.createUser(data);
  }

  @ApiOperation({ summary: 'Update user' })
  @ApiBearerAuth('JWT-Auth')
  @Patch('updateUser/:id')
  @ApiParam({ name: 'id', type: 'integer' }) // Specify parameter
  // @ApiBody({ type: createUserDto }) // Specify request body
  async updateUser(@Query() data, @Param('id', ParseIntPipe) id) {
    return await this.userService.updateUser(data, id);
  }
}
