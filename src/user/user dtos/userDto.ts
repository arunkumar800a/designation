import { ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";


export class createUserDto{
    @ApiProperty({
        description: 'Enter Name',
        example: 'Arun',
        required: true,
      })
    @IsString()
    username: string;

    @ApiProperty({
        description: 'Enter Password',
        example: '12345',
        required: true,
      })
    @IsString()
    password: string;
}