import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(private readonly prismaServie: PrismaService) {}

  async createUser(data) {
    const salt = await bcrypt.genSalt(10); // use 10 here for

    const hashedPassword = await bcrypt.hash(data.password, salt);

    const createdUser = await this.prismaServie.user_auth.create({
      data: {
        username: data.username,
        hashedPassword,
        salt,
      },
    });
    const user = await this.prismaServie.user_auth.findUnique({
      where: {
        id: createdUser.id,
      },
      select: {
        id: true,
        username: true,
      },
    });
    return user;
  }

  async updateUser(data, id) {
    const updatedUser = await this.prismaServie.user_auth.update({
      where: {
        id,
      },
      data: {
        ...data,
      },
    });
    return updatedUser;
  }
}
