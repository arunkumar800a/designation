import { ClassSerializerInterceptor, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DesignationModule } from './general/designation/designation.module';
import { PrismaModule } from './prisma/prisma.module';
import { UserModule } from './user/user.module';
import { AuthModule } from './authentication/auth/auth.module';
import { RedisModule } from './redis/redis.module';
import { APP_FILTER,  APP_INTERCEPTOR } from '@nestjs/core';
import { UserInterceptor } from './common/interceptors/user.interceptor';
import { LoggerInterceptor } from './common/interceptors/logger.interceptor';
import { HttpErrorFilter } from './common/logger/http-error.filter';
import { LoggerService } from './common/logger/logger.service';

@Module({
  imports: [
    DesignationModule,
    PrismaModule,
    UserModule,
    AuthModule,
    RedisModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,LoggerService,
    { provide: APP_INTERCEPTOR, useClass: UserInterceptor },
    { provide: APP_INTERCEPTOR, useClass: LoggerInterceptor },
    { provide: APP_FILTER, useClass: HttpErrorFilter },
    {provide: APP_INTERCEPTOR, useClass: ClassSerializerInterceptor}
  ],
})
export class AppModule {}
