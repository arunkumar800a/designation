import { Module } from '@nestjs/common';
import { DesignationService } from './designation.service';
import { DesignationController } from './designation.controller';
import { PrismaService } from 'src/prisma/prisma.service';
import { AuthModule } from 'src/authentication/auth/auth.module';

@Module({
  imports: [AuthModule],
  providers: [DesignationService, PrismaService],
  controllers: [DesignationController]
})
export class DesignationModule {}
