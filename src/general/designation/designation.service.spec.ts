

import { Test, TestingModule } from '@nestjs/testing';
import {
  NotFoundException,
  NotAcceptableException,
  ConflictException,
} from '@nestjs/common';
import { DesignationService } from './designation.service';
import { PrismaService } from '../../prisma/prisma.service';
import { SearchDesignationDto } from './designation dtos/designationDto';

describe('DepartmentService', () => {
  let designationService: DesignationService;
  let prismaService: PrismaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DesignationService, PrismaService],
    }).compile();

    designationService = module.get<DesignationService>(DesignationService);
    prismaService = module.get<PrismaService>(PrismaService);
  });

  it('should be defined', () => {
    expect(designationService).toBeDefined();
  });

  describe('createDesignationMethod', () => {
    it('should create a designation', async () => {
      const body = {
        name: 'arun',
        description: 'backend developer',
        code: 'arun123',
      };
      const user = { id: 1 }; // Mock user data
      const createDepartmentData = {
        id: 1,
        name: body.name,
        description: body.description,
        code: body.code,
        status: true,
        created_on: new Date(),
        modified_on: new Date(),
        created_by: user.id,
        modified_by: user.id,
      };

      jest
        .spyOn(prismaService.m_admin_designation, 'create')
        .mockResolvedValue(createDepartmentData);

      const result = await designationService.createDesignation(body, user);

      expect(result.name).toBe(body.name);
      expect(result.description).toBe(body.description);
      expect(result.code).toBe(body.code);
    });

    it('should handle conflict when department already exists', async () => {
      const body = {
        name: 'Test Department',
        description: 'Test Department Description',
        code: 'DEPT001',
      };
      const user = { id: 1 };

      jest
        .spyOn(prismaService.m_admin_designation, 'create')
        .mockRejectedValue(new ConflictException());

      try {
        await designationService.createDesignation(body, user);
      } catch (error) {
        expect(error).toBeInstanceOf(ConflictException);
      }
    });
  });

  describe('getAllDesignationListsMethod', () => {
    it('should get all designation lists', async () => {
      // Mock the data you expect to receive from the PrismaService
      const expectedData = [
        {
          id: 1,
          name: 'Test Department 1',
          description: 'Description 1',
          code: 'DEPT001',
          status: true,
          created_on: new Date(),
          modified_on: new Date(),
          created_by: 1,
          modified_by: 1,
        },
        {
          id: 2,
          name: 'Test Department 2',
          description: 'Description 2',
          code: 'DEPT002',
          status: true,
          created_on: new Date(),
          modified_on: new Date(),
          created_by: 1,
          modified_by: 1,
        },
      ];

      jest
        .spyOn(prismaService.m_admin_designation, 'findMany')
        .mockResolvedValue(expectedData);
      jest
        .spyOn(prismaService.m_admin_designation, 'count')
        .mockResolvedValue(expectedData.length);

      const queryDto: SearchDesignationDto = {
        page: 1,
        limit: 10,
        search: '',
        column_sort: '',
        sort_order: 'ASC',
        // advanceFilter: {}, // Add your advanceFilter value
        name: '',
        description: '',
        code: '',
        status: '',
      };

      const result = await designationService.getAllDesignation(
        queryDto,
      );

      expect(result.data.length).toBe(expectedData.length);
      expect(result.totalCount).toBe(expectedData.length);
    });

    it('should handle conflict when an error occurs', async () => {
      jest
        .spyOn(prismaService.m_admin_designation, 'findMany')
        .mockRejectedValue(new ConflictException());

      const queryDto: SearchDesignationDto = {
        page: 1,
        limit: 10,
        search: '',
        column_sort: '',
        sort_order: 'ASC',
        // advanceFilter: {}, // Add your advanceFilter value
        name: '',
        description: '',
        code: '',
        status: '',
      };

      try {
        await designationService.getAllDesignation(queryDto);
      } catch (error) {
        expect(error).toBeInstanceOf(ConflictException);
      }
    });
  });

  describe('getDepartmentListByidMethod', () => {
    it('should get a department by ID', async () => {
      const departmentId = 1; // Provide a valid department ID
      // Mock the data you expect to receive from the PrismaService
      const expectedData = {
        id: departmentId,
        name: 'Test Department',
        description: 'Test Department Description',
        code: 'DEPT001',
        status: true,
        created_on: new Date(),
        modified_on: new Date(),
        created_by: 1,
        modified_by: 1,
      };

      jest
        .spyOn(prismaService.m_admin_designation, 'findUnique')
        .mockResolvedValue(expectedData);

      const result = await designationService.getDesignationById(
        departmentId,
      );

      expect(result.id).toBe(departmentId);
    });

    it('should handle conflict when an error occurs', async () => {
      const departmentId = 1; // Provide a valid department ID

      jest
        .spyOn(prismaService.m_admin_designation, 'findUnique')
        .mockRejectedValue(new ConflictException());

      try {
        await designationService.getDesignationById(departmentId);
      } catch (error) {
        expect(error).toBeInstanceOf(ConflictException);
      }
    });
  });

  describe('updateDesignationListByIdMethod', () => {
    it('should update a department by ID', async () => {
      const departmentId = 2; // Provide a valid department ID
      const body = {
        name: 'sumit',
        description: 'Updated Department Description',
        code: 'DEPT002',
      };
      const user = { id: 1 }; // Mock user data
      // Mock the data you expect to receive from the PrismaService
      const expectedData = {
        id: departmentId,
        name: body.name,
        description: body.description,
        code: body.code,
        status: true,
        created_on: new Date(),
        modified_on: new Date(),
        created_by: user.id,
        modified_by: user.id,
      };

      jest
        .spyOn(prismaService.m_admin_designation, 'update')
        .mockResolvedValue(expectedData);

      const result = await designationService.updateDesignation(
        departmentId,
        body,
        user,
      );

      expect(result.name).toBe(body.name);
    });

    it('should handle conflict when an error occurs', async () => {
      const id = 1; // Provide a valid department ID
      const body = {
        name: 'Updated Department',
        description: 'Updated Department Description',
        code: 'DEPT002',
      };
      const user = { id: 1 };

      jest
        .spyOn(prismaService.m_admin_designation, 'update')
        .mockRejectedValue(new NotAcceptableException('Already Exist'));

      try {
        await designationService.updateDesignation(id, body, user);
      } catch (error) {
        expect(error).toBeInstanceOf(ConflictException);
        expect(error.message).toBe('Already Exist');
      }
    });
  });

  describe('activateDepartmentListByIdMethod', () => {
    it('should activate a department by ID', async () => {
      const id = 1; // Provide a valid department ID
      const user = { id: 1 }; // Mock user data
      // Mock the data you expect to receive from the PrismaService
      const expectedData = {
        id: id,
        name: 'Test Department',
        description: 'Test Department Description',
        code: 'DEPT001',
        status: true,
        created_on: new Date(),
        modified_on: new Date(),
        created_by: user.id,
        modified_by: user.id,
      };

      jest
        .spyOn(prismaService.m_admin_designation, 'update')
        .mockResolvedValue(expectedData);

      const result = await designationService.activateDesignation(
        id,
        user,
      );

      expect(true).toBe(true);
    });

    it('should handle conflict when the department is already activated', async () => {
      const id = 1; // Provide a valid department ID
      const user = { id: 1 }; // Mock user data
      // Mock the data you expect to receive from the PrismaService
      const expectedData = {
        id: id,
        name: 'Test Department',
        description: 'Test Department Description',
        code: 'DEPT001',
        status: true, // Department is already activated
        created_on: new Date(),
        modified_on: new Date(),
        created_by: user.id,
        modified_by: user.id,
      };

      jest
        .spyOn(prismaService.m_admin_designation, 'findUnique')
        .mockResolvedValue(expectedData);

      try {
        await designationService.activateDesignation(id, user);
      } catch (error) {
        expect(error).toBeInstanceOf(ConflictException);
      }
    });
  });

  describe('deleteDepartmentListByIdMethod', () => {
    it('should soft delete a department by ID', async () => {
      const departmentId = 2; // Provide a valid department ID
      const user = { id: 1 }; // Mock user data
      const expectedData = {
        id: departmentId,
        name: 'Test Department',
        description: 'Test Department Description',
        code: 'DEPT001',
        status: false, // Department is soft deleted
        created_on: new Date(),
        modified_on: new Date(),
        created_by: user.id,
        modified_by: user.id,
      };

      jest
        .spyOn(prismaService.m_admin_designation, 'update')
        .mockResolvedValue(expectedData);

      const result = await designationService.deleteDesignation(
        departmentId,
        user,
      );
      expect(result.id).toBe(departmentId);
    });

    it('should handle conflict when the department is already deleted', async () => {
      const id = 1; // Provide a valid department ID
      const user = { id: 1 }; // Mock user data
      // Mock the data you expect to receive from the PrismaService
      const expectedData = {
        id: id,
        name: 'Test Department',
        description: 'Test Department Description',
        code: 'DEPT001',
        status: false, // Department is already soft deleted
        created_on: new Date(),
        modified_on: new Date(),
        created_by: user.id,
        modified_by: user.id,
      };

      jest
        .spyOn(prismaService.m_admin_designation, 'findUnique')
        .mockResolvedValue(expectedData);

      try {
        await designationService.deleteDesignation(id, user);
      } catch (error) {
        expect(error).toBeInstanceOf(ConflictException);
        expect(error.message).toBe(`${id} already deleted`);
      }
    });
  });

  describe('checkDesignationById', () => {
    it('should check if a designation exists by ID', async () => {
      const departmentId = 1; // Provide a valid department ID
      // Mock the data you expect to receive from the PrismaService
      const expectedData = {
        id: departmentId,
        name: 'Test Department',
        description: 'Test Department Description',
        code: 'DEPT001',
        status: true,
        created_on: new Date(),
        modified_on: new Date(),
        created_by: 1,
        modified_by: 1,
      };

      jest
        .spyOn(prismaService.m_admin_designation, 'findUnique')
        .mockResolvedValue(expectedData);

      const result = await designationService.checkDesignationExistsOrNot(departmentId);

      expect(result.id).toBe(departmentId);
    });

    it('should handle not found error when the department does not exist', async () => {
      const departmentId = 1; // Provide an invalid department ID

      jest
        .spyOn(prismaService.m_admin_designation, 'findUnique')
        .mockResolvedValue(null);

      try {
        await designationService.checkDesignationExistsOrNot(departmentId);
      } catch (error) {
        expect(error).toBeInstanceOf(NotFoundException);
      }
    });
  });
});