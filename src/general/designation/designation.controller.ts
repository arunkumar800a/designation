import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { DesignationService } from './designation.service';
import { ParseIntPipe } from '@nestjs/common/pipes';
import { User } from '../../common/decorators/user.decorator';
import { AuthGuard } from '../../common/guards/auth.guard';
import {
  CreateDesignationDto,
  DesignationResponseDto,
  searchModuleDto,
} from './designation dtos/designationDto';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

@ApiTags('Designation')
@ApiBearerAuth('JWT-Auth')
@ApiResponse({ status: 403, description: 'Forbidden resource' })
// @ApiResponse({ status: 200, description: 'Successful response' })
@ApiResponse({ status: 409, description: 'Conflict' })
@ApiResponse({ status: 500, description: 'Internal server error' })
@ApiResponse({ status: 400, description: 'Bad request' })
@Controller('admin/designation')
export class DesignationController {
  constructor(private readonly designatinService: DesignationService) {}

  @ApiOperation({ summary: 'Create a new designation' })
  @UseGuards(AuthGuard)
  @Post('createDesignation')
  // @ApiBody({ type: CreateDesignationDto }) // Specify request
  @ApiResponse({ status: 201, description: 'Successful response' })
  async createDesignation(@Query() data: CreateDesignationDto, @User() user):Promise<DesignationResponseDto> {
    console.log('data', data, 'user');
    return await this.designatinService.createDesignation(data, user);
  }

  @ApiOperation({ summary: 'Get All Designations List' })
  @UseGuards(AuthGuard)
  @Get('getAllDesignationList')
  @ApiResponse({ status: 200, description: 'Successful response' })
  async getAllDesignations(@Query() paginationDto: searchModuleDto): Promise<{data: DesignationResponseDto[], totalCount: number}> {
    return await this.designatinService.getAllDesignation(paginationDto);
  }

  @ApiOperation({ summary: 'Get Designation By Id' })
  @UseGuards(AuthGuard)
  @Get('getDesignationById/:id')
  @ApiParam({ name: 'id', type: 'integer' }) // Specify parameter
  @ApiResponse({ status: 200, description: 'Successful response' })
  async getDesignationById(@Param('id', ParseIntPipe) id):Promise<DesignationResponseDto> {
    return await this.designatinService.getDesignationById(id);
  }

  @ApiOperation({ summary: 'Update Designation By Id' })
  @UseGuards(AuthGuard)
  @Patch('updateDesignationById/:id')
  @ApiParam({ name: 'id', type: 'integer' }) // Specify parameter
  // @ApiBody({ type: CreateDesignationDto }) // Specify request body
  @ApiResponse({ status: 200, description: 'Successful response' })
  async updateDesignation(
    @Query() data: CreateDesignationDto,
    @Param('id', ParseIntPipe) id,
    @User() user,
  ) :Promise<DesignationResponseDto>{
    return await this.designatinService.updateDesignation(data, id, user);
  }

  @ApiOperation({ summary: 'Inactivate Designation By Id' })
  @UseGuards(AuthGuard)
  @Delete('deleteDesignationById/:id')
  @ApiParam({ name: 'id', type: 'integer' }) // Specify parameter
  @ApiResponse({ status: 200, description: 'Successful response' })
  async deleteDesignationById(@Param('id', ParseIntPipe) id, @User() user) {
    return await this.designatinService.deleteDesignation(id, user);
  }

  @ApiOperation({ summary: 'Activate Designation By Id' })
  @UseGuards(AuthGuard)
  @Patch('activate/:id')
  @ApiParam({ name: 'id', type: 'integer' }) // Specify parameter
  @ApiResponse({ status: 200, description: 'Successful response' })
  async activateDesignation(@Param('id', ParseIntPipe) id, @User() user):Promise<DesignationResponseDto> {
    return await this.designatinService.activateDesignation(id, user);
  }
}
