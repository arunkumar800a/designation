import { IsBoolean, IsDate, IsOptional, IsPositive, IsString } from 'class-validator';
import { Exclude, Expose, Transform } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

export class CreateDesignationDto {
  @ApiProperty({
    description: 'Enter Department Name',
    example: 'Data Analyst',
    required: true,
  })
  @IsString()
  name: string;

  @ApiProperty({
    description: 'Enter Description',
    example: 'Analyzes data to derive insights and support decision-making.',
    required: false,
  })
  @IsString()
  description: string;

  @ApiProperty({
    description: 'Enter Code',
    example: 'abc',
    required: true,
  })
  @IsString()
  code: string;
}

export class searchModuleDto {
  @ApiProperty({ required: false, type: Number, description: 'Page number' })
  @ApiProperty({ required: false })
  @IsOptional()
  @IsPositive()
  page?: number;

  @ApiProperty({ required: false, type: Number, description: 'Limit of items per page' })
  @ApiProperty({ required: false })
  @IsOptional()
  @IsPositive()
  limit?: number;

  @ApiProperty({ required: false, type: String, description: 'Search term' })
  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  search?: string;

  @ApiProperty({ required: false, type: String, description: 'Column to sort by' })
  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  column_sort?: string;

  @ApiProperty({ required: false, type: String, description: 'Sort order (ASC or DESC)' })
  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  sort_order?: 'ASC' | 'DESC';

  @ApiProperty({ required: false, type: String, description: 'Name of the designation' })
  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  name: string;

  @ApiProperty({ required: false, type: String, description: 'Description of the designation' })
  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  description: string;

  @ApiProperty({ required: false, type: String, description: 'Code of the designation' })
  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  code: string;

  @ApiProperty({ required: false, type: String, description: 'Status of the designation (0 or 1)' })
  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  status: string;
  // @Expose()
  // @Transform((value) => (value ? 1 : 0))
  // @IsBoolean()

}

export class DesignationResponseDto {
  id: number;
  name: string;
  description: string;
  code: string;
  status: boolean;
  created_by: number;
  modified_by: number;
  created_on: Date;
  modified_on: Date;

  @Exclude()
  created_user: {
    username: string;
  };

  @Exclude()
  modified_user: {
    username: string;
  };

  @Expose({ name: 'created_by_username' })
  transformCreatedUsername() {
    return this.created_user.username;
  }

  @Expose({ name: 'modified_by_username' })
  transformModifiedUsername() {
    return this.modified_user.username;
  }

  constructor(partial: Partial<DesignationResponseDto>) {
    Object.assign(this, partial);
  }
}

export class SearchDesignationDto {
  @ApiProperty({ required: false })
  @IsOptional()
  @IsPositive()
  page?: number;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsPositive()
  limit?: number;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  search?: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  column_sort?: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  sort_order?: 'ASC' | 'DESC';

  // @IsOptional()
  // @IsObject()
  // advanceFilter: object;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  name: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  description: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  code: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  status: string;
  // @ApiProperty({ required: false })
  // @Expose()
  // @Transform(({ value }) => value === '1')
  // @IsIn(['0', '1'])
  // status: string;
}