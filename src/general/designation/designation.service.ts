import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { DesignationResponseDto, searchModuleDto } from './designation dtos/designationDto';
import { PrismaService } from '../../prisma/prisma.service';

const selectData = {
  id: true,
  name: true,
  description: true,
  code: true,
  status: true,
  created_by: true,
  modified_by: true,
  created_on: true,
  modified_on: true,
  created_user: {
    select: {
      username: true,
    },
  },
  modified_user: {
    select: {
      username: true,
    },
  },
};
@Injectable()
export class DesignationService {
  close // Specify the starting row here
    () {
    throw new Error('Method not implemented.');
  }
  constructor(private readonly prismaService: PrismaService) {}

  async createDesignation(body, user): Promise<DesignationResponseDto> {
    try {
      const createdDesignation =
        await this.prismaService.m_admin_designation.create({
          data: {
            name: body.name,
            description: body.description,
            code: body.code,
            created_by: user.id,
            modified_by: user.id,
          },
        });
      const designation = await this.getDesignationById(createdDesignation.id);
      return designation;
    } catch (error) {
      throw new ConflictException('Value Already Exists');
    }
  }

  async getAllDesignation(paginationDto: searchModuleDto): Promise<{
    data: DesignationResponseDto[];
    totalCount: number;
  }> {

    const {
      page = 1,
      limit = 50,
      search,
      column_sort = "created_on",
      sort_order,
      name,
      description,
      code,
      status,
    } = paginationDto;
    const skip = (page - 1) * limit;  

    let statusDB = true
    if(status === '0'){
      statusDB = false
    }
  
    console.log(statusDB, typeof(status))
    try {
      const allDesignationsRecords = await this.prismaService.m_admin_designation.findMany({
        select: {
          ...selectData,
        },
        where: {
          ...(search ? {
            OR: [
              {
                name: { contains: search },
              },
              {
                description: { contains: search },
              },
              {
                code: { contains: search },
              },
            ]
          } : {}),
          ...(name ? { name: { contains: name } } : {}),
          ...(description ? { description: { contains: description } } : {}),
          ...(code ? { code: { contains: code } } : {}),
          status: statusDB
          // Add other conditions as needed
        },
        orderBy: {
          [column_sort]: sort_order || 'desc',
        },
        skip, // Specify the starting row here
        take: limit, // Calculate the number of rows you want to retrieve
      });

      const totalCount = await this.prismaService.m_admin_designation.count({
        where: {
          ...(search ? {
            OR: [
              {
                name: { contains: search },
              },
              {
                description: { contains: search },
              },
              {
                code: { contains: search },
              },
            ]
          } : {}),
          ...(name ? { name: { contains: name } } : {}),
          ...(description ? { description: { contains: description } } : {}),
          ...(code ? { code: { contains: code } } : {}),
          status: statusDB,
          // Add other conditions as needed
        },
      });

      const data = allDesignationsRecords.map(
        (designation) => new DesignationResponseDto(designation),
      );
      return { data, totalCount };
    } catch (error) {
      console.log(error)
      const errorMessage = error.message;
    // Send the errorMessage to the frontend
      throw new ConflictException(errorMessage);
    }
  }

  async getDesignationById(id): Promise<DesignationResponseDto> {
    const designationExists = await this.checkDesignationExistsOrNot(id);

    try {
      const designationRecord =
        await this.prismaService.m_admin_designation.findUnique({
          where: {
            id,
          },
          select: {
            ...selectData,
          },
        });

      return new DesignationResponseDto(designationRecord);
    } catch (error) {
      throw new ConflictException();
    }
  }

  async updateDesignation(data, id, user): Promise<DesignationResponseDto> {
    const designationExists = await this.checkDesignationExistsOrNot(id);
    try {
      const updateDesignation =
        await this.prismaService.m_admin_designation.update({
          where: {
            id,
          },
          data: {
            ...data,
            modified_by: user.id,
          },
        });
      const designation = await this.getDesignationById(id);
      return designation;
    } catch (error) {
      throw new ConflictException('Value Already Exists');
    }
  }

  async deleteDesignation(id, user) {
    const designationExists = await this.checkDesignationExistsOrNot(id);
    try {
      const result = await this.prismaService.m_admin_designation.update({
        where: {
          id,
        },
        data: {
          status: false,
          modified_by: user.id,
        },
      });

      return { id };
    } catch (error) {
      throw new ConflictException();
    }
  }

  async activateDesignation(id, user): Promise<DesignationResponseDto> {
    const designationExists = await this.checkDesignationExistsOrNot(id);
    try {
      const result = await this.prismaService.m_admin_designation.update({
        where: {
          id,
        },
        data: {
          status: true,
          modified_by: user.id,
        },
      });
      const designation = await this.getDesignationById(id);
      return designation;
    } catch (error) {
      throw new ConflictException('Something went wrong');
    }
  }

  async checkDesignationExistsOrNot(id) {
    try {
      const result = await this.prismaService.m_admin_designation.findUnique({
        where: {
          id,
        },
      });
      if (!result)
        throw new NotFoundException('There is no designation in this id');
      return result;
    } catch (error) {
      throw new ConflictException('Something went wrong');
    }
  }
}


 // const allDesignationsRecords =
      //   await this.prismaService.m_admin_designation.findMany({
      //     select: {
      //       ...selectData,
      //     },
      //     orderBy: {
      //       created_on: 'desc',
      //     },
      //     skip, // Specify the starting row here
      //     take: limit, // Calculate the number of rows you want to retrieve
      //   }); 




      // const allDesignationsRecords = await this.prismaService.m_admin_designation.findMany({
      //   select: {
      //     ...selectData,
      //   },
      //   where: {
      //     name: { contains: name.trim()  },
      //     description: { contains: description },
      //     code: { contains: code },
      //     OR: [
      //       {
      //         name: { contains: search },
      //       },
      //       {
      //         description: { contains: search },
      //       },
      //       {
      //         code: { contains: search },
      //       },
      //     ],
      //     // Add other conditions as needed
      //   },
      //   orderBy: {
      //     [column_sort]: sort_order || 'desc', // Specify the sorting column and order
      //   },
      //   skip, // Specify the starting row here
      //   take: limit, // Calculate the number of rows you want to retrieve
      // });