import { Test, TestingModule } from '@nestjs/testing';
import { DesignationController } from './designation.controller';
import { DesignationService } from './designation.service';
import { CreateDesignationDto, DesignationResponseDto } from './designation dtos/designationDto';
import { PrismaService } from '../../prisma/prisma.service';
import { AuthenticationService } from '../../authentication/auth/auth.service';
import { JwtService } from '@nestjs/jwt';
import { RedisService } from '../../redis/redis.service';
import { RedisModule } from '../../redis/redis.module';

const returnValue = {
  id: 18,
  name: "Data Analyst",
  description: "Analyzes data to derive insights and support decision-making.",
  code: "abc",
  status: true,
  created_by: 1,
  modified_by: 1,
  created_user: { username: 'Arun' },
  modified_user: { username: 'Arun' },
  transformCreatedUsername: () => 'Arun',
  transformModifiedUsername: () => 'Arun',
} as unknown as DesignationResponseDto

describe('DesignationController', () => {
  let controller: DesignationController;
  let service: DesignationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DesignationController],
      providers: [DesignationService, PrismaService, AuthenticationService, JwtService, RedisService ],
      imports: [RedisModule]
    }).compile();

    controller = module.get<DesignationController>(DesignationController);
    service = module.get<DesignationService>(DesignationService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create a new designation', async () => {
    const createDesignationDto = {
      // Provide valid data for createDesignation
      "name": "Data Analyst",
      "description": "Analyzes data to derive insights and support decision-making.",
      "code": "abc",
    };
    const user = {
      // Provide user data
      username: "Arun",
      password: "12345"
    };

    jest.spyOn(service, 'createDesignation').mockResolvedValue(returnValue);

    const result = await controller.createDesignation(createDesignationDto, user);

    expect(result).toEqual(returnValue);
  });


  it('should get all designations', async () => {
    let ASC;
    const searchModuleDto = {
      page: 1,
      limit: 10,
      search: '',
      column_sort: '',
      sort_order: ASC,
      // advanceFilter: {}, // Add your advanceFilter value
      name: '',
      description: '',
      code: '',
      status: '',    };

      const getAllDesignationReturnValue = {data: [ {
        id: 18,
        name: "Data Analyst13",
        description: "Analyzes data to derive insights and support decision-making.",
        code: "abcdefghijklmnop",
        status: true,
        created_by: 1,
        modified_by: 1,
        created_on:  new Date(),
        modified_on:  new Date(),
        created_user: { username: 'Arun' },
        modified_user: { username: 'Arun' },
        transformCreatedUsername: () => 'Arun',
        transformModifiedUsername: () => 'Arun',
      },
      {
        "id": 16,
        "name": "Data Analyst12",
        "description": "Analyzes data to derive insights and support decision-making.",
        "code": "abcdefghijklmno",
        "status": true,
        "created_by": 1,
        "modified_by": 1,
        "created_on": new Date(),
        "modified_on": new Date(),
        created_user: { username: 'Arun' },
        modified_user: { username: 'Arun' },
        transformCreatedUsername: () => 'Arun',
        transformModifiedUsername: () => 'Arun',
      },],  "totalCount": 14}

    jest.spyOn(service, 'getAllDesignation').mockResolvedValue(getAllDesignationReturnValue);

    const result = await controller.getAllDesignations(searchModuleDto);

    expect(result).toEqual(getAllDesignationReturnValue);
  });

  it('should get a designation by ID', async () => {
    const id = 1; // Provide a valid designation ID

    jest.spyOn(service, 'getDesignationById').mockResolvedValue(returnValue);

    const result = await controller.getDesignationById(id);

    expect(result).toEqual(returnValue);
  });

  it('should update a designation by ID', async () => {
    const updateDesignationDto = {
      // Provide valid data for updating the designation
      "name": "Data Analyst",
      "description": "Analyzes data to derive insights and support decision-making.",
      "code": "abc",
    };
    const id = 18; // Provide a valid designation ID
    const user = {
      username: "Arun",
      password: "12345"
    };

    jest.spyOn(service, 'updateDesignation').mockResolvedValue({
      "id": 18,
      "name": "Data Analyst",
      "description": "Analyzes data to derive insights and support decision-making.",
      "code": "abc",
      "status": true,
      "created_by": 1,
      "modified_by": 1,
      "created_on": new Date(),
      "modified_on": new Date(),
      "created_by_username": "Arun",
      "modified_by_username": "Arun"
    } as unknown as DesignationResponseDto);

    const result = await controller.updateDesignation(new CreateDesignationDto, id, user);

    expect(result).toEqual(returnValue);
  });

  it('should delete a designation by ID', async () => {
    const id = 1; // Provide a valid designation ID
    const user = {
      // Provide user data
      username: "Arun",
      password: "12345"
    };

    jest.spyOn(service, 'deleteDesignation').mockResolvedValue(returnValue);

    const result = await controller.deleteDesignationById(id, user);

    expect(result).toEqual(returnValue);
  });

  it('should activate a designation by ID', async () => {
    const id = 18; // Provide a valid designation ID
    const user = {
      username: "Arun",
      password: "12345"
    };

    jest.spyOn(service, 'activateDesignation').mockResolvedValue(returnValue);

    const result = await controller.activateDesignation(id, user);

    expect(result).toEqual(returnValue);
  });
});
