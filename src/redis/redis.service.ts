import { Injectable, Inject } from '@nestjs/common';
import { Redis } from 'ioredis';

@Injectable()
export class RedisService {
  constructor(@Inject('REDIS_CLIENT') private readonly redisClient: Redis) {}

  async saveJwtToken(username: string, refresh_token: string) {
    try {
      // Set the JWT token with the specified key (username)
      const saveredisResult = await this.redisClient.set(
        username,
        refresh_token,
      );
      // console.log({ saveredisResult })
      return { saveredisResult };
    } catch (error) {
      console.error('Error saving JWT token to Redis:', error);
      throw error;
    }
  }

  async getRefreshToken(username: string) {
    try {
      const getRefToken = await this.redisClient.get(username);
      // console.log({ getRefToken })
      return { getRefToken };
    } catch (error) {
      console.error('Error getting refresh token from Redis:', error);
      throw error;
    }
  }
}
