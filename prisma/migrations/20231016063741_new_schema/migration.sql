-- CreateTable
CREATE TABLE `m_admin_designation` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(191) NOT NULL,
    `description` VARCHAR(191) NOT NULL,
    `code` VARCHAR(191) NOT NULL,
    `status` BOOLEAN NOT NULL DEFAULT true,
    `created_by` INTEGER NOT NULL,
    `modified_by` INTEGER NOT NULL,
    `created_on` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `modified_on` DATETIME(3) NOT NULL,

    UNIQUE INDEX `m_admin_designation_name_key`(`name`),
    UNIQUE INDEX `m_admin_designation_code_key`(`code`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `user_auth` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(191) NOT NULL,
    `password` VARCHAR(191) NOT NULL,
    `refresh_token` VARCHAR(191) NULL,

    UNIQUE INDEX `user_auth_username_key`(`username`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `m_admin_logs` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `http_operation` VARCHAR(191) NOT NULL,
    `log_type` VARCHAR(191) NOT NULL,
    `time_stamp` DATETIME(3) NOT NULL,
    `status` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `m_admin_designation` ADD CONSTRAINT `m_admin_designation_created_by_fkey` FOREIGN KEY (`created_by`) REFERENCES `user_auth`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `m_admin_designation` ADD CONSTRAINT `m_admin_designation_modified_by_fkey` FOREIGN KEY (`modified_by`) REFERENCES `user_auth`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `m_admin_logs` ADD CONSTRAINT `m_admin_logs_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `user_auth`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
